define(['DataEvent/Utility/ModuleUtility'], function(ModuleUtility) {

	/**
	 * Display module.
	 *
	 * @return Display
	 */
	var Display = function() {

		/**
		 * @type Display
		 */
		var _this = this;

		/**
		 * Hide element.
		 *
		 * @param Object parameters
		 *   string targetSelector: Selector of element(s) to hide.
		 *   jQuery eventElement: Will be hidden if targetSelector is not configured.
		 *   Array hide: Module methods to run after hide.
		 * @return boolean
		 */
		this.hide = function(parameters) {
			parameters.type = 'hide';
			_this.toggle(parameters);
		};

		/**
		 * Show element.
		 *
		 * @param Object parameters
		 *   string targetSelector: Selector of element(s) to hide.
		 *   jQuery eventElement: Will be shown if targetSelector is not configured.
		 *   Array show: Module methods to run after show.
		 * @return boolean
		 */
		this.show = function(parameters) {
			parameters.type = 'show';
			_this.toggle(parameters);
		};

		/**
		 * Toggle element.
		 *
		 * @param Object parameters
		 *   string targetSelector: Selector of element(s) to toggle.
		 *   jQuery eventElement: Will be toggled if targetSelector is not configured.
		 *   string type: One of toggle, show or hide. Defaults to toggle.
		 *   bool visibility: Whether to use visibility instead of display. Defaults to false.
		 *   Array show: Module methods to run after show.
		 *   Array hide: Module methods to run after hide.
		 * @return boolean
		 */
		this.toggle = function(parameters) {
			var parameters = jQuery.extend({
				targetSelector: null,
				eventElement: null,
				type: 'toggle',
				visibility: false,
				show: null,
				hide: null
			}, parameters);
			var targetElement = ModuleUtility.getTargetElement(parameters);
			if (targetElement === null || targetElement.length === 0) {
				return;
			}
			var toggleType = parameters.type;
			switch (toggleType) {
				case 'show':
					if (parameters.visibility) {
						targetElement.css('visibility', 'visible');
					} else {
						targetElement.show();
					}
					break;
				case 'hide':
					if (parameters.visibility) {
						targetElement.css('visibility', 'hidden');
					} else {
						targetElement.hide();
					}
					break;
				default:
					if (parameters.visibility) {
						if (targetElement.css('visibility') === 'hidden') {
							targetElement.css('visibility', 'visible');
						} else {
							targetElement.css('visibility', 'hidden');
						}
					} else {
						targetElement.toggle();
					}
					if (targetElement.is(':visible')) {
						toggleType = 'show';
					} else {
						toggleType = 'hide';
					}
			}
			// Show callback
			if (parameters.show !== null && toggleType === 'show') {
				require(['DataEvent/DataEvent'], function(DataEvent) {
					DataEvent.callModuleMethods(parameters.show, {
						eventElement: parameters.eventElement
					});
				});
			}
			// Hide callback
			if (parameters.hide !== null && toggleType === 'hide') {
				require(['DataEvent/DataEvent'], function(DataEvent) {
					DataEvent.callModuleMethods(parameters.hide, {
						eventElement: parameters.eventElement
					});
				});
			}
		};


		/**
		 * Scroll to given element.
		 *
		 * @param Object parameters
		 *   string targetSelector: jQuery selector of element to scroll to.
		 *   integer topOffset: Default scroll offset from top.
		 *   integer wait: Time to wait before scrolling.
		 *   jQuery eventElement: Element triggering the event.
		 * @return void
		 */
		this.scrollTo = function(parameters) {
			var parameters = jQuery.extend({
				targetSelector: null,
				topOffset: 0,
				wait: 0,
				eventElement: null
			}, parameters);
			var targetElement = ModuleUtility.getTargetElement(parameters);
			setTimeout(function() {
				jQuery('html,body').animate({
					scrollTop: targetElement.first().offset().top - parameters.topOffset
				});
			}, parameters.wait);

		};
	};

	return new Display();
});
