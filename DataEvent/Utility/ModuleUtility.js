define(function() {

	/**
	 * ModuleUtility module
	 *
	 * @return ModuleUtility
	 */
	var ModuleUtility = function() {

		/**
		 * Get target element based on target selector when set or eventElement otherwise.
		 *
		 * @param Object parameters
		 *   string targetSelector: jQuery selector.
		 *   string context: Context within where to find the targetSelector. Set to 'eventElement' to use eventElement. Prefix with 'closest:' to find closest element.
		 *   string contextType: Set to closest to 
		 *   jQuery eventElement: Element triggering the event.
		 * @return jQuery
		 */
		this.getTargetElement = function(parameters) {
			var parameters = jQuery.extend({
				targetSelector: null,
				context: null,
				contextType: '',
				eventElement: null
			}, parameters);
			var targetElement = null;
			if (parameters.targetSelector !== null) {
				if (parameters.context !== null) {
					var context = null;
					if (parameters.context === 'eventElement') {
						context = parameters.eventElement;
					} else if (parameters.contextType === 'closest') {
						context = parameters.eventElement.closest(parameters.context);
					} else {
						context = jQuery(parameters.context);
					}
					targetElement = context.find(parameters.targetSelector);
				} else {
					targetElement = jQuery(parameters.targetSelector);
				}
			} else if (parameters.eventElement !== null) {
				targetElement = parameters.eventElement;
			}
			return targetElement;
		};
	};

	return new ModuleUtility();
});
