define(['DataEvent/Utility/ModuleUtility', 'jquery'], function(ModuleUtility) {

	/**
	 * DataEvent base module.
	 * Run methods from RequireJS modules via data attributes linked to events using jQuery.
	 * Usage: data-<event>='[{
	 *     "module": "DataEvent/Content/Display",
	 *     "method": "hide",
	 *     "parameters": {
	 *         "targetSelector": "#id"
	 *     }
	 * }, {
	 *     <another module method>
	 * }]'
	 *
	 * @return DataEvent
	 */
	var DataEvent = function() {

		/**
		 * @type DataEvent
		 */
		var _this = this;

		/**
		 * Supported events.
		 *
		 * @type Array
		 */
		var events = [
			'click',
			'dblclick',
			'change',
			'blur',
			'focus',
			'submit',
			'reset',
			'select',
			'keydown',
			'keypress',
			'keyup',
			'mousedown',
			'mouseenter',
			'mouseleave',
			'mousemove',
			'mouseover',
			'mouseout',
			'mouseup'
		];

		/**
		 * Initialize data events.
		 *
		 * @param jQuery context May be used to initialize data events for content loaded via AJAX for instance. Defaults to whole document.
		 * @return void
		 */
		this.initializeDataEvents = function(context) {
			if (typeof context === 'undefined') {
				context = jQuery(document);
			}
			// Initialize events from data attributes
			for (var i = 0; i < events.length; i++) {
				var event = events[i];
				var selector = '[data-' + event + ']';
				context.on(event + '.dataevent', selector, _this.dataEventHandler);
			}
			jQuery(document).ready(function() {
				_this.initializeLoadDataEvent(context);
			});
		};

		/**
		 * Initialize load data event
		 *
		 * @param jQuery context See initializeDataEvents.
		 * @return void
		 */
		this.initializeLoadDataEvent = function(context) {
			context.find('[data-load]').each(function() {
				var loadEvent = {type: 'load'};
				_this.dataEventHandler(loadEvent, jQuery(this));
			});
			_this.preloadModules(context);
		};

		

		/**
		 * Data event handler.
		 *
		 * @param Object event
		 * @param jQuery element
		 * @return bool
		 */
		this.dataEventHandler = function(event, element) {
			if (typeof element === 'undefined') {
				var element = jQuery(this);
			}
			// Retrieve configured event handlers / module methods from corresponding data attribute
			var moduleMethods = element.data(event.type);
			return _this.callModuleMethods(moduleMethods, {eventElement: element});
		};

		/**
		 * Call one or multiple module methods.
		 * This method should also be used to handle nested module method callbacks from within other modules.
		 *
		 * @param Array moduleMethods
		 * @param Object parameters Default parameters like the eventElement.
		 * @return bool
		 */
		this.callModuleMethods = function(moduleMethods, parameters) {
			if (!jQuery.isArray(moduleMethods)) {
				console.error('Module methods should be defined in an array');
				return false;
			}
			var returnValue = false;
			for (var i = 0; i < moduleMethods.length; i++) {
				var moduleMethod = moduleMethods[i];
				// Merge custom parameters if available
				if (typeof parameters === 'object') {
					if (moduleMethod.hasOwnProperty('parameters') && typeof moduleMethod.parameters === 'object') {
						moduleMethod.parameters = jQuery.extend({}, moduleMethod.parameters, parameters);
					} else {
						moduleMethod.parameters = parameters;
					}
				}
				// If true(ish) value is returned allow the default action to occur
				if (_this.callModuleMethod(moduleMethod)) {
					returnValue = true;
				}
			}
			return returnValue;
		};

		/**
		 * Call a module method.
		 *
		 * @param Object parameters
		 *   string module: Module to load.
		 *   string method: Method to call.
		 *   mixed parameters: Method parameters.
		 *   boolean returnValue: Set to true(ish) value to allow the default action to occur.
		 * @return boolean
		 */
		this.callModuleMethod = function(parameters) {
			var parameters = jQuery.extend({
				module: null,
				method: null,
				parameters: {},
				returnValue: false
			}, parameters);
			// Check the module data
			if (parameters.module === null || parameters.method === null) {
				console.error('Module and/or method not configured');
				return;
			}
			// Load the RequireJS module and call the method
			require([parameters.module], function(Module) {
				var callMethod = Module[parameters.method];
				if (typeof callMethod === 'function') {
					callMethod(parameters.parameters);
				} else {
					// Note: This warning may occur because of JavaScript cache in Firefox
					console.warn('Module ' + parameters.module + ' does not have a method ' + parameters.method);
				}
			});
			return parameters.returnValue;
		};

		/**
		 * Whether given object is a module method.
		 *
		 * @param Object moduleMethod
		 * @return bool
		 */
		this.isModuleMethod = function(moduleMethod) {
			if (moduleMethod === null
			|| typeof moduleMethod !== 'object'
			|| !moduleMethod.hasOwnProperty('module')
			|| !moduleMethod.hasOwnProperty('method')) {
				return false;
			}
			return true;
		};

		/**
		 * Preload modules.
		 *
		 * @param jQuery context See initializeDataEvents.
		 * @return void
		 */
		this.preloadModules = function(context) {
			// Collect modules for each event type
			var modules = {};
			for (var i = 0; i < events.length; i++) {
				var event = events[i];
				modules = _this.getDataModules(context, event, modules);
			}
			// Convert to array and load the modules
			var loadModules = [];
			for (var module in modules) {
				loadModules.push(module);
			}
			require(loadModules);
		};

		/**
		 * Get data modules.
		 *
		 * @param jQuery context See initializeDataEvents.
		 * @param string dataKey lowerCamelCase data key.
		 * @param Object modules
		 * @return Object Updated modules object.
		 */
		this.getDataModules = function(context, dataKey, modules) {
			// Find elements with data event
			var dataAttribute = _this.lowerCamelCaseToHyphens(dataKey);
			var selector = '[data-' + dataAttribute + ']';
			var eventElements = context.find(selector);

			// Collect modules from all data event elements
			eventElements.each(function() {
				var eventElement = jQuery(this);
				var moduleMethods = eventElement.data(dataKey);
				if (!jQuery.isArray(moduleMethods)) {
					console.error('Module methods should be defined in an array');
					return true;
				}
				// Collect modules from configured module methods
				for (var i = 0; i < moduleMethods.length; i++) {
					var moduleMethod = moduleMethods[i];
					if (_this.isModuleMethod(moduleMethod)) {
						// Collect distinct modules
						modules[moduleMethod.module] = moduleMethod.module;
						// Add nested modules
						modules = _this.getNestedDataModules(moduleMethod, modules);
					}
				}
			});
			return modules;
		};

		/**
		 * Get nested data modules.
		 *
		 * @param Object moduleMethod
		 * @param Object modules
		 * @return Object updated modules object.
		 */
		this.getNestedDataModules = function(moduleMethod, modules) {
			// Continue only if parameters are present
			if (!moduleMethod.hasOwnProperty('parameters')
			|| moduleMethod.parameters === null
			|| typeof moduleMethod.parameters !== 'object') {
				return modules;
			}

			// Find nested module methods within parameters
			for (var parameter in moduleMethod.parameters) {
				var nestedModuleMethods = moduleMethod.parameters[parameter];
				// Module methods should be in an array
				if (!jQuery.isArray(nestedModuleMethods)) {
					continue;
				}
				for (var i = 0; i < nestedModuleMethods.length; i++) {
					var nestedModuleMethod = nestedModuleMethods[i];
					if (_this.isModuleMethod(nestedModuleMethod)) {
						// Collect distinct modules
						modules[nestedModuleMethod.module] = nestedModuleMethod.module;
						// Recursion to find deeper nested modules
						modules = _this.getNestedDataModules(nestedModuleMethod, modules);
					}
				}
			}

			// Also retrieve custom trigger data events
			if (moduleMethod.hasOwnProperty('method')
			&& moduleMethod.method === 'triggerDataEvent'
			&& moduleMethod.parameters.hasOwnProperty('dataKey')) {
				// Use whole document as context since the element may be anywhere
				modules = _this.getDataModules(jQuery(document), moduleMethod.parameters.dataKey, modules);
			}
			return modules;
		};

		/**
		 * Convert lower camel case to lower case hyphens.
		 *
		 * @param string value
		 * @return string
		 */
		this.lowerCamelCaseToHyphens = function(value) {
			return value.replace(/([a-z][A-Z])/g, function(g) {
				return g[0] + '-' + g[1].toLowerCase();
			});
		};

		/**
		 * Trigger data event from custom data attribute(s).
		 * Usage: data-<event>='[{
		 *     "module": "DataEvent/DataEvent",
		 *     "method": "triggerDataEvent",
		 *     "parameters": {
		 *         "targetSelector": "a",
		 *         "dataKey": "doSomething"
		 *     }
		 * }]'
		 * Note that the data key "doSomething" will match the attribute "data-do-something".
		 *
		 * @param Object parameters
		 *   string targetSelector: jQuery selector of element(s) to trigger.
		 *   string|Array dataKey: lowerCamelCase data key(s) to trigger data event from.
		 *   jQuery eventElement: Element triggering the event.
		 * @return void
		 */
		this.triggerDataEvent = function(parameters) {
			var parameters = jQuery.extend({
				targetSelector: null,
				dataKey: null,
				eventElement: null
			}, parameters);
			if (parameters.dataKey === null) {
				return;
			}
			var targetElement = ModuleUtility.getTargetElement(parameters);
			var dataKeys = parameters.dataKey;
			if (!jQuery.isArray(dataKeys)) {
				dataKeys = [dataKeys];
			}
			for (var i = 0; i < dataKeys.length; i++) {
				var dataKey = dataKeys[i];
				var moduleMethods = targetElement.data(dataKey);
				if (!(typeof moduleMethods === 'undefined')) {
					parameters.triggerElement = parameters.eventElement;
					parameters.eventElement = targetElement;
					_this.callModuleMethods(moduleMethods, parameters);
				}
			}
		};

		/**
		 * Bind events manually, allowing you to bind events to multiple elements via a container element.
		 * Usage: data-load='[{
		 *     "module": "DataEvent/DataEvent",
		 *     "method": "bindEvent",
		 *     "parameters": {
		 *         "targetSelector": "a",
		 *         "targetEvent": "click",
		 *         "moduleMethods: [...]
		 *     }
		 * }]'
		 *
		 * @param Object parameters
		 *   jQuery eventElement: Element triggering the request and used as container element if available.
		 *   string targetSelector: jQuery selector for child elements to bind the event to.
		 *   string targetEvent: Event on which to trigger the module methods (click by default).
		 *   Array moduleMethods: Module methods to call on event.
		 * @return void
		 */
		this.bindEvent = function(parameters) {
			var parameters = jQuery.extend({
				eventElement: null,
				targetSelector: null,
				targetEvent: 'click',
				moduleMethods: null
			}, parameters);
			if (parameters.targetSelector === null) {
				console.error('Parameter targetSelector is required');
			}
			var context;
			if (parameters.eventElement === null) {
				context = jQuery(document);
			} else {
				context = parameters.eventElement;
			}
			context.on(parameters.targetEvent + '.bindevent', parameters.targetSelector, function() {
				return _this.callModuleMethods(parameters.moduleMethods, {eventElement: jQuery(this)});
			});
		};
	};

	// Initialize DataEvent module and return it
	var instance = new DataEvent();
	instance.initializeDataEvents();
	return instance;
});
