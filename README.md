# Data event #

Run methods from RequireJS modules via data attributes linked to events using jQuery.

## Setup ##

* Add the files from this repository to your JavaScript directory.
* Add the following script tag to the HTML head section:
```
#!HTML

<script data-main="JavaScript/RequireDataEvent" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.2.0/require.min.js"></script>
```
* Update the data-main attribute to point to your RequireJS initialization file (if needed).
* Update the baseUrl setting within the RequireJS initialization file (RequireDataEvent.js).

## Example usage:

```
#!HTML

<a id="link" href="#test" data-click='[{
	"module": "DataEvent/Content/Display",
	"method": "toggle",
	"parameters": {
		"targetSelector": "#toggle"
	}
}]'>toggle</a>
<div id="toggle">Now you see me</div>
```


## Other configuration ##

### Multiple JavaScript locations ###

If you're using multiple JavaScript locations in your project, change or remove the baseUrl setting. Then update the paths to point to the correct paths.

### CDN fallback ###

If the CDN is unavailable or not accessible because of networking policies for instance you still want to the JavaScript to load. This can be done using a fallback. Add the following script tag to the HTML head section below the "normal" RequireJS script tag:

```
#!HTML

<script>console.log(window);window.requirejs || document.write('<script data-main="JavaScript/RequireDataEvent" src="JavaScript/require.min.js">\x3C/script>')</script>
```
Update the paths to point to the correct locations for your project. Then update the paths within the RequireJS initialization file. For jQuery this should be:

```
#!javascript

paths: {
	jquery: [
		'//code.jquery.com/jquery-2.2.2.min',
		'jquery-2.2.2.min'
	]
}
```
Again, update the path to the correct location for your project.